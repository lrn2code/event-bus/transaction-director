package org.painsomnia.txndirector.receptionist.dao.po

import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document("guests")
data class GuestPo(
    val name: String,
    val id: ObjectId? = null,
    val createdAt: LocalDateTime = LocalDateTime.now()
)
