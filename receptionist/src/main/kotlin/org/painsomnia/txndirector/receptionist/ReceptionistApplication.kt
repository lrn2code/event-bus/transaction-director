package org.painsomnia.txndirector.receptionist

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ReceptionistApplication

fun main(args: Array<String>) {
    runApplication<ReceptionistApplication>(args = args)
}
