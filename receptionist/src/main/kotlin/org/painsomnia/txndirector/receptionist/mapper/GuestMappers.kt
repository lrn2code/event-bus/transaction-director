package org.painsomnia.txndirector.receptionist.mapper

import org.painsomnia.txndirector.core.dto.GuestDto
import org.painsomnia.txndirector.receptionist.dao.po.GuestPo

fun GuestPo.toDto() = GuestDto(this.id!!.toHexString(), this.name, this.createdAt)
