package org.painsomnia.txndirector.receptionist.config

import kotlinx.coroutines.runBlocking
import org.painsomnia.txndirector.receptionist.mapper.toDto
import org.painsomnia.txndirector.receptionist.service.GuestService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class EventBusConfig(
    private val guestService: GuestService,
) {
    @Bean
    fun guestPassedDoor() = { guestName: String ->
        runBlocking {
            guestService.register(guestName).toDto()
        }
    }

    @Bean
    fun problemOccurred() = { id: String ->
        runBlocking {
            guestService.handleProblematicGuest(id)
        }
    }
}
