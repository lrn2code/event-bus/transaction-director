package org.painsomnia.txndirector.receptionist.dao.dao

import org.bson.types.ObjectId
import org.painsomnia.txndirector.receptionist.dao.po.GuestPo
import org.springframework.data.repository.kotlin.CoroutineCrudRepository

interface GuestRepository : CoroutineCrudRepository<GuestPo, ObjectId>
