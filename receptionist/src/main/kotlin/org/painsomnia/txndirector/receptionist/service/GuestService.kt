package org.painsomnia.txndirector.receptionist.service

import org.bson.types.ObjectId
import org.painsomnia.txndirector.core.event.Topics
import org.painsomnia.txndirector.receptionist.dao.dao.GuestRepository
import org.painsomnia.txndirector.receptionist.dao.po.GuestPo
import org.springframework.cloud.stream.function.StreamBridge
import org.springframework.stereotype.Service

@Service
class GuestService(
    private val streamBridge: StreamBridge,
    private val guestRepository: GuestRepository,
) {
    suspend fun register(guestName: String) = guestRepository.save(GuestPo(guestName))

    suspend fun handleProblematicGuest(id: String) = ObjectId(id)
        .let { guestRepository.findById(it) }
        .also { guestRepository.delete(it!!) }
        .let { streamBridge.send(Topics.RECEPTIONIST_GUEST_DLQ.topic, it!!.name) }
}
