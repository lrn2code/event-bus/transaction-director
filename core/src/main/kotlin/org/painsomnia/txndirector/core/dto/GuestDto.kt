package org.painsomnia.txndirector.core.dto

import java.time.LocalDateTime

data class GuestDto(
    val id: String,
    val name: String,
    val createdAt: LocalDateTime,
)
