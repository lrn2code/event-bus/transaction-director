package org.painsomnia.txndirector.core.event

enum class Topics(val topic: String) {
    GUEST_PASSED_DOOR("guest-passed-door"),
    PORTER_LUGGAGE_DLQ("porter.luggage.dlq"),
    RECEPTIONIST_GUEST_DLQ("receptionist.guest.dlq")
}
