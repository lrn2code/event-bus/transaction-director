import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") apply false
    id("org.jlleitschuh.gradle.ktlint")
    id("io.gitlab.arturbosch.detekt")
}

group = "org.painsomnia.transaction-director"
version = "0.1.0-SNAPSHOT"

subprojects {
    group = "org.painsomnia"
    version = "0.0.1-SNAPSHOT"

    apply { plugin("jacoco") }
    apply { plugin("org.jetbrains.kotlin.jvm") }
    apply { plugin("org.jlleitschuh.gradle.ktlint") }
    apply { plugin("io.gitlab.arturbosch.detekt") }

    configure<JavaPluginExtension> {
        sourceCompatibility = JavaVersion.VERSION_17
        withSourcesJar()
    }

    repositories {
        mavenCentral()
    }

    dependencies {
        val implementation by configurations
        val testImplementation by configurations

        implementation("org.jetbrains.kotlin:kotlin-reflect")
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

        testImplementation("org.mockito.kotlin:mockito-kotlin:4.1.0")
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = "17"
        }
    }

    tasks.withType<Test> {
        useJUnitPlatform()
    }

    tasks.withType<JacocoReport> {
        dependsOn(tasks.withType(Test::class.java))
    }
}
