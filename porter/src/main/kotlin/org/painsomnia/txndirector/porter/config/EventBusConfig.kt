package org.painsomnia.txndirector.porter.config

import kotlinx.coroutines.runBlocking
import org.painsomnia.txndirector.core.dto.GuestDto
import org.painsomnia.txndirector.porter.service.LuggageService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import kotlin.random.Random

private const val MIN_ROOM_NUMBER = 1
private const val MAX_ROOM_NUMBER = 101

@Configuration
class EventBusConfig(
    private val luggageService: LuggageService,
) {
    @Bean
    fun transferLuggage() = { input: GuestDto ->
        runBlocking {
            luggageService.transfer(input, Random.nextInt(MIN_ROOM_NUMBER, MAX_ROOM_NUMBER))
        }
    }
}
