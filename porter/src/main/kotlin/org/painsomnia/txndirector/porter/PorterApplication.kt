package org.painsomnia.txndirector.porter

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PorterApplication

fun main(args: Array<String>) {
    runApplication<PorterApplication>(args = args)
}
