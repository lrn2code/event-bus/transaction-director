package org.painsomnia.txndirector.porter.dao.repository

import org.bson.types.ObjectId
import org.painsomnia.txndirector.porter.dao.po.Luggage
import org.springframework.data.repository.kotlin.CoroutineCrudRepository

interface LuggageRepository : CoroutineCrudRepository<Luggage, ObjectId>
