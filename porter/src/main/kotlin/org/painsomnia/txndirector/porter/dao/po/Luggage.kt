package org.painsomnia.txndirector.porter.dao.po

import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.mapping.Document

@Document("luggage")
data class Luggage(
    val guestId: String,
    val room: Int,
    val id: ObjectId? = null,
)
