package org.painsomnia.txndirector.porter.service

import mu.KotlinLogging
import org.painsomnia.txndirector.core.dto.GuestDto
import org.painsomnia.txndirector.core.event.Topics
import org.painsomnia.txndirector.porter.dao.po.Luggage
import org.painsomnia.txndirector.porter.dao.repository.LuggageRepository
import org.springframework.cloud.stream.function.StreamBridge
import org.springframework.stereotype.Service
import kotlin.random.Random

private val logger = KotlinLogging.logger { }

@Service
class LuggageService(
    private val luggageRepository: LuggageRepository,
    private val streamBridge: StreamBridge,
) {
    suspend fun transfer(guest: GuestDto, room: Int) {
        if (Random.nextBoolean()) {
            luggageRepository.save(Luggage(guest.id, room))
        } else {
            logger.warn { "oopsie! a problematic guest" }
            streamBridge.send(Topics.PORTER_LUGGAGE_DLQ.topic, guest.id)
        }
    }
}
