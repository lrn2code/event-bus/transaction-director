rootProject.name = "transaction-director"

pluginManagement {
    val kotlinVersion = "1.8.0"

    plugins {
        kotlin("jvm") version kotlinVersion
        kotlin("plugin.spring") version kotlinVersion
        kotlin("kapt") version kotlinVersion
        id("org.springframework.boot") version "3.0.1"
        id("io.spring.dependency-management") version "1.1.0"
        id("org.jlleitschuh.gradle.ktlint") version "10.3.0"
        id("io.gitlab.arturbosch.detekt") version "1.20.0"
        id("org.graalvm.buildtools.native") version "0.9.18"
    }
}

include(
    "core",
    "doorman",
    "porter",
    "receptionist"
)
