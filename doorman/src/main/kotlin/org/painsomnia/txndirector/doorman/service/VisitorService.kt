package org.painsomnia.txndirector.doorman.service

import org.painsomnia.txndirector.core.event.Topics
import org.springframework.cloud.stream.function.StreamBridge
import org.springframework.stereotype.Service

@Service
class VisitorService(
    private val streamBridge: StreamBridge,
) {
    suspend fun letIn(name: String) = streamBridge.send(Topics.GUEST_PASSED_DOOR.topic, name)
}
