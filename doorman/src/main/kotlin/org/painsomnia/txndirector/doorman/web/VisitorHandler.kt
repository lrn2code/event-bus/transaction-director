package org.painsomnia.txndirector.doorman.web

import jakarta.annotation.PostConstruct
import org.painsomnia.txndirector.doorman.service.VisitorService
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.awaitBody
import org.springframework.web.reactive.function.server.bodyValueAndAwait
import org.springframework.web.reactive.function.server.json

@Component
class VisitorHandler(
    private val visitorService: VisitorService,
) {

    @PostConstruct
    fun greet() = println("hi there")

    suspend fun letIn(request: ServerRequest) = visitorService
        .letIn(request.awaitBody())
        .let { ok().json().bodyValueAndAwait(it) }
}
