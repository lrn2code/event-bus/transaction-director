package org.painsomnia.txndirector.doorman

import org.painsomnia.txndirector.doorman.config.appBeans
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DoormanApplication

fun main(args: Array<String>) {
    runApplication<DoormanApplication>(args = args) {
        addInitializers(appBeans)
    }
}
