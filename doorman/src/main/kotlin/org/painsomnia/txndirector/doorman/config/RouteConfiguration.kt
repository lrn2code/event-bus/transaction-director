package org.painsomnia.txndirector.doorman.config

import org.painsomnia.txndirector.doorman.web.VisitorHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.http.MediaType.TEXT_PLAIN
import org.springframework.web.reactive.function.server.coRouter

@Configuration
class RouteConfiguration {
    @Bean
    fun doormanRouter(visitorHandler: VisitorHandler) = coRouter {
        accept(APPLICATION_JSON, TEXT_PLAIN).and("/door")
            .nest {
                POST("/visit", visitorHandler::letIn)
            }
    }
}
