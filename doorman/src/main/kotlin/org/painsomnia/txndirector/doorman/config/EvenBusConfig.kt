package org.painsomnia.txndirector.doorman.config

import mu.KotlinLogging
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

private val logger = KotlinLogging.logger { }

@Configuration
class EvenBusConfig {

    @Bean
    fun problemOccurred() = { guest: String ->
        logger.error { "$guest, I hope to never see your face again" }
    }
}
